#include "gamePong.h"
#include <asf.h>


//Posi��es
int initial_position[5] = {4, 5, 6, 7, 8};
int positionPlayer1[5] = {4, 5, 6, 7, 8};
int positionPlayer2[5] = {4, 5, 6, 7, 8};
int positionBall[2] = {(NUM_LINHA / 2) - 1 , (NUM_COLUNA / 2) - 1};

//Variaveis Auxiliares
int flag_Inverse = 0;
int flag_CheckBorder = 0;
int falg_SumPos = 0;

int cont = 0;
int move_Path = 2;
int score_Player1 = 0;
int score_Player2 = 0;


void move_Player(int player, int value, uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	if(player == 1){
		if(value == 1 && positionPlayer1[4] < NUM_LINHA - 1){
			led[positionPlayer1[0]][0] = ERASE;
			for(int i = 0; i < 5; i++){
				positionPlayer1[i]+= 1; 
			}
			
		}
		else if(value == 3 && positionPlayer1[0] > 0){
			led[positionPlayer1[4]][0] = ERASE;
			for(int i = 0; i < 5; i++){
				positionPlayer1[i] -=  1; 
			}
			
		}

	}
	
	else if(player == 2){
		if(value == 1 && positionPlayer2[4] < NUM_LINHA - 1){
			led[positionPlayer2[0]][NUM_COLUNA - 1] = ERASE;
			for(int i = 0; i < 5; i++){
				positionPlayer2[i] += 1; 
			}
			
		}
		else if(value == 3 && positionPlayer2[0] > 0){
			led[positionPlayer2[4]][NUM_COLUNA - 1] = ERASE;
			for(int i = 0; i < 5; i++){
				positionPlayer2[i] -= 1; 
			}
		}
	}
}

void atualiza_Player(int player, uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	//Player 1	
	if(player == 1){
		for(int i = 0; i < 5; i++){
			led[positionPlayer1[i]][0] = BLUE;
		}
	}

	//Player 2
	else if(player == 2){
		for(int i = 0; i < 5; i++){
			led[positionPlayer2[i]][NUM_COLUNA - 1] = GREEN;
		}
	}
}


int move_Ball(uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	
	if(!flag_CheckBorder){
		led[positionBall[0]][positionBall[1]] = ERASE;
	}
	
	switch(move_Path){
		
		//Player 1
		case(1):
			if(flag_Inverse){
				positionBall[0] += 1;
			}
			else{
				positionBall[0] -= 1;
			}
			
			positionBall[1] += 1;
			break;

		case(2):
			positionBall[1] += 1;
			break;
		
		case(3):
			if(flag_Inverse){
				positionBall[0] -= 1;
			}
			else{
				positionBall[0] += 1;
			}
			positionBall[1] += 1;
			break;
		
		//Player 2
		case(4):
			if(flag_Inverse){
				positionBall[0] += 1;
			}
			else{
				positionBall[0] -= 1;
			}
			positionBall[1] -= 1;
			break;

		case(5):
			positionBall[1] -= 1;
			break;
		
		case(6):
			if(flag_Inverse){
				positionBall[0] -= 1;
			}
			else{
				positionBall[0] += 1;
			}
			positionBall[1] -= 1;
			break;
		
	}
	int var = check_Border(led);
	
	if(!flag_CheckBorder){
		led[positionBall[0]][positionBall[1]] = RED;		
	}
	
	if (var){
		return 1;
	}
		
	return 0;
}

	
int check_Border(uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	
	if(positionBall[1] == 0){
		for(int i = 0; i < 5; i++ ){
			if(positionBall[0] == positionPlayer1[i]){
				
				if (i == 0 || i == 1){
					move_Path = 1;
				}
				else if (i == 2){
					move_Path = 2;
				}
				if (i == 3 || i == 4){
					move_Path = 3;
				}
				
				flag_Inverse = 0;
				flag_CheckBorder = 1;
				break;
			}
			cont += 1;
		}			
		if (cont == 5){
			score_Player2 += 1;
			return 1;
		}
		cont = 0;		
	}
	
	else if(positionBall[1] == NUM_COLUNA - 1){
		for(int i = 0; i < 5; i++ ){
			if(positionBall[0] == positionPlayer2[i]){
				if (i == 0 || i == 1){
					move_Path = 4;
				}
				else if (i == 2){
					move_Path = 5;
				}
				if (i == 3 || i == 4){
					move_Path = 6;
				}
				
				flag_CheckBorder = 1;		
				flag_Inverse = 0;
				break;  
			}
			cont += 1;
		}
		if (cont == 5){
			score_Player1 += 1;
			return 1;
		}
		cont = 0;
	}
	
	else{
		flag_CheckBorder = 0;
	}
	
	if(positionBall[0] == 0 || positionBall[0] == NUM_LINHA - 1){
		flag_Inverse = 1;
	}
	
	return 0;
}

void reset_Led(uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	for(int i = 0; i < NUM_LINHA; i++){
		for(int j = 0; j < NUM_COLUNA; j++){
			led[i][j] = ERASE;
		}
	}
}

void reset_Game(uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	cont = 0;
	
	for(int i = 0; i < 5; i++){
		positionPlayer1[i] = initial_position[i];
		positionPlayer2[i] = initial_position[i];
	}
	
	positionBall[0] = (NUM_LINHA  / 2) - 1;
	positionBall[1] = (NUM_COLUNA / 2) - 1;
	move_Path = 2;
	
	atualiza_Player(1, led);
	atualiza_Player(2, led);
	
	if(score_Player1 == 3 || score_Player2 == 3){
		score_Player1 = 0;
		score_Player2 = 0;
	}
	
	
	delay_s(1);
	
}

void start_Pong(uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	//PLAYER
	atualiza_Player(1, led);
	atualiza_Player(2, led);
}

void end_Animation(int player, uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	
	if(player == 1){
		led[7][13] = RED;
		led[7][14] = RED;
		led[7][15] = RED;
				
		led[8][14] = RED;
		led[9][14] = RED;
		led[10][14] = RED;
		led[11][14] = RED;
				
		led[10][13] = RED;
	}
	
	else if(player == 2){
		led[7][12] = RED;
		led[7][13] = RED;
		led[7][14] = RED;
		led[7][15] = RED;
				
		led[8][13] = RED;
		led[9][14] = RED;
		led[10][15] = RED;
		led[11][14] = RED;
				
		led[11][13] = RED;
		led[10][12] = RED;

	}
	
	// P
	led[7][4] = RED;
	led[8][4] = RED;
	led[9][4] = RED;
	led[10][4] = RED;
	led[11][4] = RED;
	
	led[11][5] = RED;
	led[11][6] = RED;
	
	led[10][6] = RED;
	led[9][6] = RED;
	
	led[9][5] = RED;
	
	//--
	
	led[9][9] = RED;
	led[9][10] = RED;
	
	// W
	led[2][5] = RED;
	led[3][6] = RED;
	led[2][7] = RED;
	
	
	led[3][4] = RED;
	led[4][4] = RED;
	led[5][4] = RED;
	
	
	led[3][8] = RED;
	led[4][8] = RED;
	led[5][8] = RED;
	
	// i
	led[2][10] = RED;
	led[3][10] = RED;
	led[4][10] = RED;
	led[5][10] = RED;
	
	//N
	led[2][12] = RED;
	led[3][12] = RED;
	led[4][12] = RED;
	led[5][12] = RED;
	
	led[4][13] = RED;
	led[3][14] = RED;
	
	
	led[2][15] = RED;
	led[3][15] = RED;
	led[4][15] = RED;
	led[5][15] = RED;
}


void score_animation(uint32_t *led[NUM_LINHA][NUM_COLUNA]){
	if(score_Player1 == 3){
		end_Animation(1, led);	
	}
	
	else if(score_Player2 == 3){
		end_Animation(2, led);
	}
	
	else{
		
		led[4][9] = RED;
		led[4][10] = RED;
	
		if(score_Player1 == 0){
			led[2][4] = RED;
			led[2][5] = RED;
		
		
			led[3][3] = RED;
			led[4][3] = RED;
			led[5][3] = RED;
		
		
			led[6][4] = RED;
			led[6][5] = RED;
		
			led[3][6] = RED;
			led[4][6] = RED;
			led[5][6] = RED;
		}
	
		else if(score_Player1 == 1){
			led[2][4] = RED;
			led[2][5] = RED;
			led[2][6] = RED;
		
			led[3][5] = RED;
			led[4][5] = RED;
			led[5][5] = RED;
			led[6][5] = RED;
		
			led[5][4] = RED;
		}
	
		else if(score_Player1 == 2){
			led[2][3] = RED;
			led[2][4] = RED;
			led[2][5] = RED;
			led[2][6] = RED;
	
			led[3][4] = RED;
			led[4][5] = RED;
			led[5][6] = RED;
			led[6][5] = RED;
	
			led[6][4] = RED;		
			led[5][3] = RED;	
		}
			
		if(score_Player2 == 0){
			led[2][14] = RED;
			led[2][15] = RED;
	
	
			led[3][13] = RED;
			led[4][13] = RED;
			led[5][13] = RED;
				
			
			led[6][14] = RED;
			led[6][15] = RED;
				
			led[3][16] = RED;
			led[4][16] = RED;
			led[5][16] = RED;
		}
	
		else if(score_Player2 == 1){
			led[2][13] = RED;
			led[2][14] = RED;
			led[2][15] = RED;
		
			led[3][14] = RED;
			led[4][14] = RED;
			led[5][14] = RED;
			led[6][14] = RED;
		
			led[5][13] = RED;
		}
	
		else if(score_Player2 == 2){
			led[2][12] = RED;
			led[2][13] = RED;
			led[2][14] = RED;
			led[2][15] = RED;
		
			led[3][13] = RED;
			led[4][14] = RED;
			led[5][15] = RED;
			led[6][14] = RED;
		
			led[6][13] = RED;
			led[5][12] = RED;
		}
	
	}
}


/*
void game_Over(){
	bool borda = false;
	for(int i = 0; i < NUM_COLUNA; i++){
		if(positionBall[0] == i){
			borda = true;
			break;
		}
	}

	if (positionBall[1] == NUM_LINHA - 1 && borda && led[positionBall[0]][positionBall[1]] == ERASE){
		return true;
	}

	borda = false;
}
*/