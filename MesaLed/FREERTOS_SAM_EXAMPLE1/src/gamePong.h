#include <asf.h>

#define BLUE   0x00FF0000
#define RED    0x0000FF00
#define GREEN  0x000000FF
#define ERASE  0x00000000

#define NUM_LINHA 14
#define NUM_COLUNA 20

void move_Player(int player, int value, uint32_t *led[NUM_LINHA][NUM_COLUNA]);
void atualiza_Player(int player, uint32_t *led[NUM_LINHA][NUM_COLUNA]);
void start_Pong(uint32_t *led[NUM_LINHA][NUM_COLUNA]);
int move_Ball(uint32_t *led[NUM_LINHA][NUM_COLUNA]);
int  check_Border(uint32_t *led[NUM_LINHA][NUM_COLUNA]);
void reset_Game(uint32_t *led[NUM_LINHA][NUM_COLUNA]);
void reset_Led(uint32_t *led[NUM_LINHA][NUM_COLUNA]);
void score_animation(uint32_t *led[NUM_LINHA][NUM_COLUNA]);

	